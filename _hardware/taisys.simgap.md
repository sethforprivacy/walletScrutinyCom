---
title: Taisys SIMGap®
appId: taisys.simgap
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: Taisys
providerWebsite: https://www.taisys.com
website: 
shop: 
country: TW
price: 
repository: 
issue: 
icon: taisys.simgap.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-12-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 

---

## Product Description

This product was mentioned in a [press release](http://taisys.io/news-detail?lang=&id=39d4VQN4kyFspmAwER3_9V6sTZbvIP1IYQQkIBdjyw).

It is described as:

- Open Source
- With NIST FIPS 140-2 Level 3 certification
- With Android SDK for developers
- Should act as the secure element for Web3

We have not been able to find a publicly available version for sale on the web. 

We reached out to them via email at simgap <at> taisys.io, and at contact <at> taisys.com. 
