---
title: CoinKite Q1
appId: coinkite.q1
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 120
- 75
- 22
weight: 
provider: CoinKite, Inc.
providerWebsite: https://coinkite.com/
website: https://coldcard.com/docs/coldcard-q/
shop: https://store.coinkite.com/store/cc-q1
country: CA
price: 199.99 USD
repository: 
issue: 
icon: coinkite.q1.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-12-06
signer: 
reviewArchive: 
twitter: Coinkite
social: 
features: 

---

## Product Description 

- [View Documentation](https://coldcard.com/docs/coldcard-q/)
  > - The Q uses exactly the same security model as the Mk4 COLDCARD, with dual multi-vendor secure elements.
  > - QWERTY Keyboard: ideal for long BIP-39 passphrases.
  > - 320x240 pixel LCD screen, 3.2" diagonal size. Four times Mk4 size.
  > - Battery powered by 3x AAA cells (or USB). Airgapped and/or wireless!
  > - Dual MicroSD slots (push-pull type, not spring loaded).
  > - QR Code scanner done right™, with LED illumination and advanced scanning algorithms and serial interface.
  > - NFC communication, like Mk4
  > - Includes internal storage for spare MicroSD cards.
  > - USB data & NFC data can be irreversibly blocked, by cutting a PCB trace: it permanently disable USB data and/or NFC data.

## Analysis 

This product is still in reservation status and not yet officially released.