---
wsId: Paxful
title: Paxful | Bitcoin Wallet
altTitle: 
authors:
- leo
appId: com.paxful.wallet
appCountry: 
idd: 1443813253
released: 2019-05-09
updated: 2024-03-12
version: 2.8.17
stars: 3.4
reviews: 2941
size: '65271808'
website: https://paxful.com
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: paxful
social:
- https://www.facebook.com/paxful
- https://www.reddit.com/r/paxful
features: 
developerName: Paxful Inc

---

{% include copyFromAndroid.html %}
