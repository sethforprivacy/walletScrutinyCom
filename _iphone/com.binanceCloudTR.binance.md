---
wsId: BinanceTR
title: 'Binance TR: Bitcoin ve Kripto'
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2024-03-11
version: 2.4.1
stars: 4.7
reviews: 88713
size: '158935040'
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BinanceTR
social:
- https://www.facebook.com/TRBinanceTR
features: 
developerName: BN TEKNOLOJİ ANONİM ŞİRKETİ

---

{% include copyFromAndroid.html %}
