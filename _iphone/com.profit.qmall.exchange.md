---
wsId: qmallExchange
title: Qmall - єдина українська біржа
altTitle: 
authors:
- danny
appId: com.profit.qmall.exchange
appCountry: gb
idd: '1600467380'
released: 2021-12-21
updated: 2023-09-06
version: 1.1.48
stars: 5
reviews: 8
size: '24714240'
website: https://qmall.io/
repository: 
issue: 
icon: com.profit.qmall.exchange.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: QmallExchange
social:
- https://www.linkedin.com/company/qmallexchange/about/
- https://www.facebook.com/qmall.io
- https://www.youtube.com/channel/UCRfKNWczZ84ASCSEIsaLCag
features: 
developerName: QMALL Exchange

---

{% include copyFromAndroid.html %}
