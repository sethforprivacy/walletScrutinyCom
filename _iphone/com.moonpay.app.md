---
wsId: moonPayBuyBitcoin
title: 'MoonPay: Buy Bitcoin, Ethereum'
altTitle: 
authors:
- danny
appId: com.moonpay.app
appCountry: us
idd: '1635031432'
released: 2023-04-17
updated: 2024-04-02
version: 1.12.22
stars: 4.3
reviews: 967
size: '68175872'
website: https://www.moonpay.com
repository: 
issue: 
icon: com.moonpay.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-07-18
signer: 
reviewArchive: 
twitter: moonpay
social:
- https://www.linkedin.com/company/moonpay
- https://www.instagram.com/moonpay
- https://www.facebook.com/officialmoonpay
features: 
developerName: MoonPay

---

{% include copyFromAndroid.html %}