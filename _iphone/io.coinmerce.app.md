---
wsId: coinmerce
title: Coinmerce - Buy Bitcoin
altTitle: 
authors:
- danny
appId: io.coinmerce.app
appCountry: nl
idd: '1409599830'
released: 2018-07-29
updated: 2024-03-19
version: 5.8.5
stars: 3.5
reviews: 20
size: '171895808'
website: https://coinmerce.io/en/
repository: 
issue: 
icon: io.coinmerce.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: coinmerce
social:
- https://www.facebook.com/CoinmerceNL
- https://www.linkedin.com/company/coinmerce/
features: 
developerName: Coinmerce BV

---

{% include copyFromAndroid.html %}

