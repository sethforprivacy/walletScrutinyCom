---
wsId: revixBuyBitcoin
title: Altify - Crypto Investment Hub
altTitle: 
authors:
- danny
appId: com.revix.mobile
appCountry: za
idd: '1590491829'
released: 2022-08-01
updated: 2024-04-12
version: 1.8.0
stars: 4.2
reviews: 22
size: '78274560'
website: https://www.altify.app/
repository: 
issue: 
icon: com.revix.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-09
signer: 
reviewArchive: 
twitter: RevixInvest
social:
- https://www.facebook.com/RevixInvest
- https://www.linkedin.com/company/revixinvest
- https://www.instagram.com/revixinvest
- https://medium.com/revixinvestment
- https://t.me/revixinvest
- https://www.tiktok.com/@revixinvest
features: 
developerName: Revix UK Ltd

---

{% include copyFromAndroid.html %}
