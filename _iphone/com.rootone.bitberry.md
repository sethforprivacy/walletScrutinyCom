---
wsId: bitberry
title: 'Bitberry : Safe Wallet'
altTitle: 
authors:
- danny
appId: com.rootone.bitberry
appCountry: us
idd: 1411817291
released: 2018-10-09
updated: 2023-02-01
version: 1.4.2
stars: 4.1
reviews: 7
size: '89378816'
website: http://bitberry.app
repository: 
issue: 
icon: com.rootone.bitberry.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2024-01-28
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: RootOne

---

{% include copyFromAndroid.html %}
