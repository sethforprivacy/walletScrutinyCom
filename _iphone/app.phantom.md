---
wsId: phantomlabs
title: Phantom - Crypto Wallet
altTitle: 
authors:
- danny
appId: app.phantom
appCountry: us
idd: '1598432977'
released: 2022-01-30
updated: 2024-04-08
version: 24.5.0
stars: 4.5
reviews: 1605
size: '76374016'
website: https://phantom.app
repository: 
issue: 
icon: app.phantom.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-12-21
signer: 
reviewArchive: 
twitter: phantom
social:
- https://www.reddit.com/r/phantom/
- https://www.linkedin.com/company/phantomwallet/
- https://www.youtube.com/@phantom-wallet
- https://github.com/phantom-labs
features: 
developerName: Phantom Technologies Incorporated

---

{% include copyFromAndroid.html %}
