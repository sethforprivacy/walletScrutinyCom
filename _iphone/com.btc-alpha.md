---
wsId: BTCAlpha
title: 'BTC-Alpha: Buy Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.btc-alpha
appCountry: us
idd: 1437629304
released: 2019-04-20
updated: 2022-09-25
version: 1.15.1
stars: 4.3
reviews: 11
size: '97477632'
website: http://btc-alpha.com
repository: 
issue: 
icon: com.btc-alpha.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-09-20
signer: 
reviewArchive: 
twitter: btcalpha
social:
- https://www.linkedin.com/company/btcalpha
- https://www.facebook.com/btcalpha
features: 
developerName: Alphacom Uab, dba ALPCOM

---

{% include copyFromAndroid.html %}


