---
wsId: bitkeep
title: Bitget Wallet, ex BitKeep
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2024-04-05
version: 8.13.1
stars: 4.6
reviews: 1220
size: '172177408'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom
features: 
developerName: BitKeep Global Inc.

---

 {% include copyFromAndroid.html %}
