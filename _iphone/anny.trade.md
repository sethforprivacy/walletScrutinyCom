---
wsId: annyTrade
title: Anny.trade
altTitle: 
authors:
- danny
appId: anny.trade
appCountry: gb
idd: '1642936782'
released: 2023-02-28
updated: 2024-04-09
version: 1.4.3
stars: 0
reviews: 0
size: '48841728'
website: 
repository: 
issue: 
icon: anny.trade.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-12-02
signer: 
reviewArchive: 
twitter: AnnyCrypto
social: 
features: 
developerName: Anny.trade

---

{% include copyFromAndroid.html %}
