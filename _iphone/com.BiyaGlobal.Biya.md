---
wsId: biyaGlobal
title: BIYA Global
altTitle: 
authors:
- danny
appId: com.BiyaGlobal.Biya
appCountry: sn
idd: '1546702297'
released: 2021-02-09
updated: 2024-03-28
version: 2.8.35
stars: 0
reviews: 0
size: '63992832'
website: 
repository: 
issue: 
icon: com.BiyaGlobal.Biya.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: 
social:
- https://www.biyagl.com
features: 
developerName: BIYA GLOBAL LLC

---

{% include copyFromAndroid.html %}
