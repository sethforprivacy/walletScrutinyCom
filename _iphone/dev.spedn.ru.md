---
wsId: Koshelek
title: Кошелек. Криптовалюты & Токены
altTitle: 
authors:
- danny
appId: dev.spedn.ru
appCountry: ru
idd: 1524167720
released: 2020-08-05
updated: 2024-02-28
version: 1.15.6
stars: 4.3
reviews: 101
size: '84397056'
website: https://koshelek.ru/
repository: 
issue: 
icon: dev.spedn.ru.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-01-01
signer: 
reviewArchive: 
twitter: koshelek_ru
social:
- https://www.facebook.com/koshelekru
features: 
developerName: Кошелёк.ру

---

{% include copyFromAndroid.html %}
