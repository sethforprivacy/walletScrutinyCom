---
wsId: 
title: CoinGlass - Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.xiandanxiaohai.Bybt
appCountry: us
idd: 1522250001
released: 2020-07-08
updated: 2024-04-05
version: 1.9.3
stars: 4.9
reviews: 1884
size: '22346752'
website: https://www.coinglass.com
repository: 
issue: 
icon: com.xiandanxiaohai.Bybt.jpg
bugbounty: 
meta: ok
verdict: fake
date: 2021-11-02
signer: 
reviewArchive: 
twitter: coinglass_com
social: 
features: 
developerName: Coinglass Technology Co., Limited

---

{% include copyFromAndroid.html %}
