---
wsId: probosWallet
title: Probos Wallet
altTitle: 
authors:
- danny
appId: io.probos.support
appCountry: ua
idd: '1615751706'
released: 2022-09-20
updated: 2022-10-01
version: '1.1'
stars: 5
reviews: 1
size: '53679104'
website: https://probos.io/
repository: 
issue: 
icon: io.probos.support.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2023-10-03
signer: 
reviewArchive: 
twitter: probos_wallet
social:
- https://t.me/probos_info
- https://www.facebook.com/proboswallet
- https://www.instagram.com/proboswallet
features: 
developerName: ELEPHANTSLAB LP

---

{% include copyFromAndroid.html %}