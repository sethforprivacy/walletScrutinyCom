---
wsId: snowballDeFi
title: Snowball Smart DeFi Wallet
altTitle: 
authors:
- danny
appId: money.snowball.app
appCountry: us
idd: '1449662311'
released: 2019-07-18
updated: 2024-03-09
version: 3.1.1
stars: 4.3
reviews: 136
size: '37137408'
website: https://www.snowball.money
repository: 
issue: 
icon: money.snowball.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-15
signer: 
reviewArchive: 
twitter: snowball_money
social: 
features: 
developerName: Snowball Finance

---

{% include copyFromAndroid.html %}