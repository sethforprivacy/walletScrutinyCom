---
wsId: ottrSolWallet
title: Ottr Wallet
altTitle: 
authors:
- danny
appId: finance.ottr.app
appCountry: us
idd: '1628669270'
released: 2022-09-25
updated: 2023-12-20
version: '1.58'
stars: 4.6
reviews: 11
size: '101213184'
website: https://ottr.finance
repository: 
issue: 
icon: finance.ottr.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Ottr Finance Inc.

---

{% include copyFromAndroid.html %}
