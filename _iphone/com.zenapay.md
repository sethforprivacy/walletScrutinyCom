---
wsId: ZenaPay
title: ZenaPay
altTitle: 
authors:
- danny
appId: com.zenapay
appCountry: us
idd: '1292806306'
released: 2017-10-26
updated: 2023-10-30
version: '5.0'
stars: 4.9
reviews: 7
size: '16259072'
website: https://www.zenapay.com/Product#ZenaPayWallet
repository: 
issue: 
icon: com.zenapay.jpg
bugbounty: 
meta: removed
verdict: nosource
date: 2023-12-19
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/ZenaPay
- https://www.linkedin.com/company/zenapay1/about/
- https://www.youtube.com/channel/UCyehPdsSfe2MickTIsYuDFg
features: 
developerName: Epazz

---

{% include copyFromAndroid.html %}