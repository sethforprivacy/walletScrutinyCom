---
wsId: makeDelta
title: 코인미어캣 - 보조지표 알림과 스크리너
altTitle: 
authors:
- danny
appId: com.makedelta.slying
appCountry: kr
idd: '1581110050'
released: 2021-08-15
updated: 2024-01-19
version: 7.2.0
stars: 4.7
reviews: 116
size: '37757952'
website: https://www.coinmrk.com/
repository: 
issue: 
icon: com.makedelta.slying.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-14
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/profile.php?id=100083754173968
- https://www.youtube.com/channel/UCLC_CKhMggklpoHowc6TvNA
features: 
developerName: Make Delta Co., Ltd.

---

{% include copyFromAndroid.html %}
