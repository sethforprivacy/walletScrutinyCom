---
wsId: Forexcom
title: 'FOREX.com: Trade Stocks & FX'
altTitle: 
authors:
- danny
appId: com.gaincapital.forex
appCountry: gb
idd: 1506581586
released: 2020-10-14
updated: 2024-04-11
version: 1.171.4683
stars: 3.6
reviews: 49
size: '144824320'
website: https://www.forex.com/en-uk/
repository: 
issue: 
icon: com.gaincapital.forex.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: forexcom
social:
- https://www.facebook.com/FOREXcom
features: 
developerName: GAIN Capital Group LLC

---

{% include copyFromAndroid.html %}

