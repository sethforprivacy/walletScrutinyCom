---
wsId: coldStorageCoinsApp
title: Blockchain Mint
altTitle: 
authors:
- danny
appId: com.rearden-metals.Cold-Storage-Coins
appCountry: hu
idd: '1352363663'
released: 2018-03-11
updated: 2024-02-14
version: 3.10.2
stars: 0
reviews: 0
size: '19856384'
website: https://blockchainmint.com
repository: 
issue: 
icon: com.rearden-metals.Cold-Storage-Coins.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-09-06
signer: 
reviewArchive: 
twitter: 
social:
- https://t.me/BlockchainMint
features: 
developerName: Rearden Metals Pte Ltd

---

{% include copyFromAndroid.html %}