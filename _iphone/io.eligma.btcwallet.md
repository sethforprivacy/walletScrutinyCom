---
wsId: ellyCrypto
title: Elly, crypto wallet app
altTitle: 
authors:
- danny
appId: io.eligma.btcwallet
appCountry: si
idd: '1353823277'
released: 2018-09-14
updated: 2022-01-30
version: 8.0.1
stars: 4.6
reviews: 35
size: '60161024'
website: https://elly.com
repository: 
issue: 
icon: io.eligma.btcwallet.jpg
bugbounty: 
meta: obsolete
verdict: custodial
date: 2024-01-21
signer: 
reviewArchive: 
twitter: GoCrypto_
social:
- https://www.facebook.com/gocrypto.eligma
- https://t.me/eligma
- https://medium.com/eligma-blog
- https://www.youtube.com/channel/UCfshAN27bKPe4f3uBIr7jjA
features: 
developerName: Eligma d.o.o.

---

{% include copyFromAndroid.html %}