---
wsId: primalNostr
title: Primal
altTitle: 
authors:
- danny
appId: net.primal.iosapp.Primal
appCountry: us
idd: '1673134518'
released: 2023-12-01
updated: 2024-04-11
version: 1.5.2
stars: 4.9
reviews: 227
size: '30334976'
website: https://primal.net
repository: 
issue: 
icon: net.primal.iosapp.Primal.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-03-02
signer: 
reviewArchive: 
twitter: 
social: 
features:
- ln
developerName: Primal Systems Incorporated.

---

{% include copyFromAndroid.html %}