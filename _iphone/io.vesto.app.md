---
wsId: vestoApp
title: Vesto
altTitle: 
authors:
- danny
appId: io.vesto.app
appCountry: us
idd: '1529147510'
released: 2022-04-14
updated: 2022-04-26
version: 1.0.1
stars: 0
reviews: 0
size: '109371392'
website: https://vesto.io
repository: 
issue: 
icon: io.vesto.app.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2023-08-04
signer: 
reviewArchive: 
twitter: vestoprotocol
social:
- https://www.linkedin.com/company/vesto-llc
features: 
developerName: Vesto LLC

---

{% include copyFromAndroid.html %}

