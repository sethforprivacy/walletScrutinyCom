---
wsId: ruufPayWallet
title: RuufPay Swap Pay Crypto Wallet
altTitle: 
authors:
- danny
appId: app.ruufpay.ruufpay
appCountry: us
idd: '1557726087'
released: 2022-06-07
updated: 2023-07-01
version: 2.1.0
stars: 4.6
reviews: 21
size: '70030336'
website: 
repository: 
issue: 
icon: app.ruufpay.ruufpay.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-12-02
signer: 
reviewArchive: 
twitter: ruufpay
social:
- https://ruufpay.app
- https://www.linkedin.com/company/ruufpay
- https://www.facebook.com/ruufpay
- https://www.instagram.com/ruufpay
features: 
developerName: RuufPay

---

{% include copyFromAndroid.html %}