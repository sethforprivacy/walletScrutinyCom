---
wsId: TradeAries
title: 'Aries: Trading Simplified.'
altTitle: 
authors:
- danny
appId: com.tradearies.ariestrading
appCountry: ng
idd: 1554894180
released: 2021-06-29
updated: 2024-03-01
version: 3.4.4
stars: 0
reviews: 0
size: '41978880'
website: https://tradearies.com/
repository: 
issue: 
icon: com.tradearies.ariestrading.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-11-22
signer: 
reviewArchive: 
twitter: TradeAries
social:
- https://www.linkedin.com/company/tradearies
- https://www.facebook.com/TradeAries
features: 
developerName: Aries Financial, Inc.

---

{% include copyFromAndroid.html %}