---
wsId: argent
title: Argent — Starknet Wallet
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2024-03-25
version: 4.25.1
stars: 4.5
reviews: 2179
size: '138388480'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 
features: 
developerName: Argent

---

{% include copyFromAndroid.html %}