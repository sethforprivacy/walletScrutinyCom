---
wsId: simpleCrypto
title: 'Simple crypto: Buy BTC, ETH'
altTitle: 
authors:
- danny
appId: app.simple.com
appCountry: us
idd: '1603406843'
released: 2022-05-06
updated: 2023-08-09
version: 2.0.7
stars: 5
reviews: 2
size: '126097408'
website: https://simple.app/
repository: 
issue: 
icon: app.simple.com.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-09-15
signer: 
reviewArchive: 
twitter: smpl_app
social:
- https://t.me/smpl_app
- https://www.linkedin.com/company/smplapp
- https://www.facebook.com/smplapp
features: 
developerName: Simple Europe UAB

---

{% include copyFromAndroid.html %}