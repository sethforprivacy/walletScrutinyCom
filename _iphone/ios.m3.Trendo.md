---
wsId: trendofx
title: 'Trendo: Stocks & Forex Trading'
altTitle: 
authors:
- danny
appId: ios.m3.Trendo
appCountry: in
idd: 1530580389
released: 2020-09-29
updated: 2024-04-03
version: 3.5.50
stars: 5
reviews: 31
size: '56108032'
website: https://fxtrendo.com/
repository: 
issue: 
icon: ios.m3.Trendo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-04
signer: 
reviewArchive: 
twitter: 
social:
- https://www.instagram.com/fxtrendo/
features: 
developerName: Trendo LLC

---

{% include copyFromAndroid.html %}
