---
wsId: yuh
title: 'Yuh: 3-in-1 Finance App'
altTitle: 
authors:
- danny
appId: com.swissquote.Yuh
appCountry: ch
idd: '1493935010'
released: 2021-05-10
updated: 2024-04-09
version: 1.21.4
stars: 4.7
reviews: 11604
size: '122635264'
website: https://www.yuh.com
repository: 
issue: 
icon: com.swissquote.Yuh.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-02
signer: 
reviewArchive: 
twitter: yuh_app
social:
- https://www.facebook.com/yuhapp.en/
features: 
developerName: Swissquote

---

{% include copyFromAndroid.html %}
