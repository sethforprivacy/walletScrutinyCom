---
wsId: coinOneKr
title: 코인원
altTitle: 
authors:
- danny
appId: kr.co.coinone.officialapp
appCountry: kr
idd: 1326526995
released: 2018-03-27
updated: 2024-04-01
version: 4.10.2
stars: 2.7
reviews: 867
size: '133931008'
website: 
repository: 
issue: 
icon: kr.co.coinone.officialapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-17
signer: 
reviewArchive: 
twitter: CoinoneOfficial
social:
- https://coinone.co.kr
- https://www.facebook.com/coinone
features: 
developerName: Coinone

---

{% include copyFromAndroid.html %}