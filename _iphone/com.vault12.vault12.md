---
wsId: vault12
title: Bitcoin Inheritance & Backup
altTitle: 
authors:
- danny
appId: com.vault12.vault12
appCountry: us
idd: '1451596986'
released: 2019-08-28
updated: 2024-03-27
version: 2.1.0
stars: 4.8
reviews: 16
size: '53163008'
website: https://vault12.com/guard
repository: https://github.com/vault12
issue: 
icon: com.vault12.vault12.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-03-23
signer: 
reviewArchive: 
twitter: _vault12_
social:
- https://www.facebook.com/GetVault12/
- https://www.linkedin.com/company/vault12/
- https://www.instagram.com/vault12/
- https://www.youtube.com/channel/UCoH4zPOpJhq6RbTZqUqzFwA
features: 
developerName: Vault12, Inc.

---

{% include copyFromAndroid.html %}
