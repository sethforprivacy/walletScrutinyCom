---
wsId: ZuPago
title: ViZO APP
altTitle: 
authors:
- danny
appId: app.zupago.zp
appCountry: us
idd: 1565673730
released: 2021-05-10
updated: 2024-04-12
version: 1.0.62
stars: 4.5
reviews: 52
size: '26750976'
website: https://vizo.app
repository: 
issue: 
icon: app.zupago.zp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-12-19
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: ZuPago HyBrid HD Wallet

---

{% include copyFromAndroid.html %}