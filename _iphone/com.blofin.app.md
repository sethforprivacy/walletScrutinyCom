---
wsId: blofin
title: Blofin
altTitle: 
authors:
- danny
appId: com.blofin.app
appCountry: tt
idd: '1616804346'
released: 2022-04-15
updated: 2023-06-22
version: 2.4.0
stars: 0
reviews: 0
size: '130105344'
website: https://blofin.com
repository: 
issue: 
icon: com.blofin.app.jpg
bugbounty: 
meta: removed
verdict: nosendreceive
date: 2023-08-16
signer: 
reviewArchive: 
twitter: Blofin_Official
social: 
features: 
developerName: Blofin Inc.

---

{% include copyFromAndroid.html %}
