---
wsId: bitpandaBitcoin
title: 'Bitpanda: Buy BTC & crypto'
altTitle: 
authors:
- danny
appId: com.bitpanda.bitpanda
appCountry: at
idd: '1449018960'
released: 2019-11-29
updated: 2024-04-12
version: 2.72.0
stars: 4.7
reviews: 16054
size: '221901824'
website: https://www.bitpanda.com/app
repository: 
issue: 
icon: com.bitpanda.bitpanda.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-29
signer: 
reviewArchive: 
twitter: Bitpanda_global
social:
- https://www.facebook.com/BITPANDA
- https://www.reddit.com/r/bitpanda
- https://www.linkedin.com/company/bitpanda
- https://www.youtube.com/@Bitpanda_global
- https://discord.com/invite/dmM9Dz7Kt8
- https://www.instagram.com/bitpanda_global
features: 
developerName: Bitpanda GmbH

---

{% include copyFromAndroid.html %}