---
wsId: babbApp
title: BABB
altTitle: 
authors:
- danny
appId: com.babbltd.babb-ios
appCountry: us
idd: '1474391121'
released: 2020-04-14
updated: 2024-04-08
version: 2.3.4
stars: 4.4
reviews: 84
size: '35607552'
website: https://getbabb.com/
repository: 
issue: 
icon: com.babbltd.babb-ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-10
signer: 
reviewArchive: 
twitter: getbabb
social:
- https://www.linkedin.com/company/babb
- https://www.facebook.com/getbabb
- https://www.youtube.com/channel/UCQtkZd7sfzbEugz7VdFhv4Q
- https://discord.com/invite/39rQp2g6JA
- https://babb.medium.com
- https://t.me/getbabb_official
features: 
developerName: BABB Ltd

---

{% include copyFromAndroid.html %}
