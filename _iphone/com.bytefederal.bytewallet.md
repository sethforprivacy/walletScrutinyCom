---
wsId: byteWallet
title: ByteWallet
altTitle: 
authors:
- danny
appId: com.bytefederal.bytewallet
appCountry: us
idd: '1569062610'
released: 2021-07-27
updated: 2024-04-01
version: 1.5.2
stars: 3.5
reviews: 38
size: '63212544'
website: https://www.bytefederal.com/
repository: 
issue: 
icon: com.bytefederal.bytewallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-05
signer: 
reviewArchive: 
twitter: bytefederal
social:
- https://www.facebook.com/bytefederal
- https://www.instagram.com/bytefederalatm
- https://www.youtube.com/channel/UCozOzfZ0MgqLT_TA7hbNh4g
- https://www.linkedin.com/company/bytefederal
features: 
developerName: Byte Federal, Inc.

---

{% include copyFromAndroid.html %}
