---
wsId: ammerWallet
title: Ammer Wallet
altTitle: 
authors:
- danny
appId: io.trustody.wallet
appCountry: nz
idd: '1599698329'
released: 2022-03-04
updated: 2024-03-26
version: '7.1'
stars: 0
reviews: 0
size: '103708672'
website: https://ammer.cards
repository: 
issue: 
icon: io.trustody.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-20
signer: 
reviewArchive: 
twitter: AmmerCards
social: 
features: 
developerName: Ammer Technologies AG

---

{% include copyFromAndroid.html %}
