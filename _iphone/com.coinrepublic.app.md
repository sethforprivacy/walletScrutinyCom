---
wsId: coinRepublic
title: Coin Republic
altTitle: 
authors:
- danny
appId: com.coinrepublic.app
appCountry: au
idd: '1540941971'
released: 2020-11-28
updated: 2024-04-08
version: 2.1.1
stars: 5
reviews: 3
size: '29868032'
website: https://coinrepublic.exchange/
repository: 
issue: 
icon: com.coinrepublic.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-17
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: COIN REPUBLIC PTY LTD

---

{% include copyFromAndroid.html %}

