---
wsId: suniwallet
title: Suni Wallet
altTitle: 
authors:
- danny
appId: com.suniwallet.app
appCountry: us
idd: '6445960717'
released: 2023-03-06
updated: 2024-03-06
version: 2.1.0
stars: 5
reviews: 20
size: '138266624'
website: 
repository: 
issue: 
icon: com.suniwallet.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-02
signer: 
reviewArchive: 
twitter: Suni_wallet
social:
- https://www.instagram.com/suni_wallet
- https://www.youtube.com/channel/UCEimtCczYVdwvGx1SCLdhYQ
- https://www.linkedin.com/company/suni-wallet
features: 
developerName: SUNI WALLET LLC

---

{% include copyFromAndroid.html %}
