---
wsId: niceX
title: NiceX
altTitle: 
authors:
- danny
appId: com.nicehash.NiceX
appCountry: ph
idd: '1619979069'
released: 2022-07-11
updated: 2024-03-22
version: 1.4.3
stars: 5
reviews: 1
size: '60065792'
website: https://www.nicex.com
repository: 
issue: 
icon: com.nicehash.NiceX.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: NiceXExchange
social:
- https://www.facebook.com/NiceXExchange
- https://www.youtube.com/c/NiceHash_Official
- https://discord.com/invite/nicehash
features: 
developerName: H-BIT, d.o.o.

---

{% include copyFromAndroid.html %}
