---
wsId: STICPAY
title: STICPAY
altTitle: 
authors:
- danny
appId: com.sticpay.app
appCountry: us
idd: 1274956968
released: 2017-09-05
updated: 2024-03-29
version: '3.77'
stars: 4.3
reviews: 25
size: '48545792'
website: https://www.sticpay.com/
repository: 
issue: 
icon: com.sticpay.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: SticPay
social:
- https://www.linkedin.com/company/sticpay
- https://www.facebook.com/sticpay.global
features: 
developerName: STIC FINANCIAL LTD.

---

{% include copyFromAndroid.html %}
