---
wsId: OGPay
title: OGPay Business
altTitle: 
authors:
- danny
appId: com.unifiedsignal.ogpaywallet
appCountry: us
idd: 1471960731
released: 2019-08-03
updated: 2024-04-10
version: '11.6'
stars: 4.7
reviews: 224
size: '192350208'
website: https://ogpaywallet.com/
repository: 
issue: 
icon: com.unifiedsignal.ogpaywallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Original Digital Corporation

---

{% include copyFromAndroid.html %}
