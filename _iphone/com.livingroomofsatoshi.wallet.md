---
wsId: WalletofSatoshi
title: Wallet of Satoshi
altTitle: 
authors:
- leo
appId: com.livingroomofsatoshi.wallet
appCountry: 
idd: 1438599608
released: 2019-05-20
updated: 2023-08-29
version: 2.2.7
stars: 4.4
reviews: 53
size: '54358016'
website: https://walletofsatoshi.com/
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: walletofsatoshi
social:
- https://www.facebook.com/walletofsatoshi
features:
- ln
developerName: Wallet of Satoshi

---

This is a custodial wallet according to their website's FAQ:

> It is a zero-configuration custodial wallet with a focus on simplicity and the
  best possible user experience.

and therefore **not verifiable**.
