---
wsId: NetDania
title: NetDania Stock & Forex Trader
altTitle: 
authors:
- danny
appId: 4KSDE4UX92.com.netdania.quotelist
appCountry: us
idd: 446371774
released: 2011-07-01
updated: 2023-12-22
version: 4.10.5
stars: 4.7
reviews: 12959
size: '77364224'
website: http://www.netdania.com
repository: 
issue: 
icon: 4KSDE4UX92.com.netdania.quotelist.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/NetDania-146001445410373
features: 
developerName: NetDania SRL

---

{% include copyFromAndroid.html %}
