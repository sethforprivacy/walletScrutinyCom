---
wsId: bitvavo
title: Bitvavo | Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitvavo
appCountry: be
idd: 1483903423
released: 2020-05-28
updated: 2024-04-04
version: 2.39.0
stars: 4.6
reviews: 4127
size: '105089024'
website: https://bitvavo.com
repository: 
issue: 
icon: com.bitvavo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Bitvavo B.V.

---

{% include copyFromAndroid.html %}
