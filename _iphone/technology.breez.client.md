---
wsId: breez
title: 'Breez: Lightning Client & POS'
altTitle: 
authors:
- danny
appId: technology.breez.client
appCountry: us
idd: '1463604142'
released: 2022-05-01
updated: 2023-10-23
version: 1.0.24
stars: 3.8
reviews: 32
size: '188615680'
website: 
repository: https://github.com/breez/breezmobile
issue: https://github.com/breez/breezmobile/issues/247
icon: technology.breez.client.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-12-15
signer: 
reviewArchive: 
twitter: breez_tech
social:
- http://breez.technology
features: 
developerName: BREEZ DEVELOPMENT LTD
redirect_from: 

---

{% include copyFromAndroid.html %}