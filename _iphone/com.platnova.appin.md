---
wsId: platnovaApp
title: Platnova
altTitle: 
authors:
- danny
appId: com.platnova.appin
appCountry: us
idd: '1619003446'
released: 2022-04-26
updated: 2024-04-05
version: 0.8.9
stars: 3.6
reviews: 36
size: '155711488'
website: https://platnova.com
repository: 
issue: 
icon: com.platnova.appin.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-02
signer: 
reviewArchive: 
twitter: getplatnova
social:
- https://www.facebook.com/getplatnova
- https://www.instagram.com/getplatnova
- https://www.linkedin.com/company/platnova
features: 
developerName: Platnova

---

{% include copyFromAndroid.html %}