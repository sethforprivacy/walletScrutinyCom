---
wsId: AnchorUSD
title: Anchor - Buy Bitcoin and Ether
altTitle: 
authors:
- danny
appId: app.anchors.anchorusd
appCountry: us
idd: 1495986023
released: 2020-01-30
updated: 2024-04-10
version: 1.19.12
stars: 4.3
reviews: 5210
size: '40039424'
website: https://www.tryanchor.com/
repository: 
issue: 
icon: app.anchors.anchorusd.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: anchorusd
social: 
features: 
developerName: Anchor US LLC

---

{% include copyFromAndroid.html %}
