---
wsId: hashVest
title: getHashApp
altTitle: 
authors:
- danny
appId: com.hashvest.hash
appCountry: ng
idd: '6443470887'
released: 2022-11-11
updated: 2023-11-08
version: 1.0.7
stars: 0
reviews: 0
size: '119150592'
website: 
repository: 
issue: 
icon: com.hashvest.hash.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2024-01-09
signer: 
reviewArchive: 
twitter: gethashapp
social:
- https://www.facebook.com/goHashApp
- https://www.linkedin.com/company/gethashapp
- https://www.youtube.com/@hashapp
features: 
developerName: Hashvest Technologies

---

{% include copyFromAndroid.html %}