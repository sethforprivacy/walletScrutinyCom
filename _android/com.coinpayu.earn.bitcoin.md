---
wsId: 
title: Coinpayu
altTitle: 
authors:
- danny
users: 500000
appId: com.coinpayu.earn.bitcoin
appCountry: 
released: 2021-07-06
updated: 2024-03-20
version: 1.3.5
stars: 3.5
ratings: 
reviews: 167
size: 
website: https://www.coinpayu.com
repository: 
issue: 
icon: com.coinpayu.earn.bitcoin.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-10-09
signer: 
reviewArchive: 
twitter: CoinPayU
social:
- https://www.facebook.com/coinpayu
redirect_from: 
developerName: Coinpayu
features: 

---

Coinpayu offers crypto rewards in exchange for users completing tasks or viewing advertisements.

> Coinpayu is a free crypto rewards platform where you can earn crypto by viewing advertisements or doing offers.

From the [site:](https://www.coinpayu.com/bitcoin)

> Bitcoin address is like: 12t9YDPgwueZ9NyMgw519p7AA8isjr6SMw
>
> This address is used for withdrawal of your earnings.
>
> You can get your own bitcoin address in an exchange platform or a bitcoin wallet.

This app **does not work or serve as a wallet.**